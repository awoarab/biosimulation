# INF 200 BioSim projects January 2023 
#### Awo Arab & Alin Dak Al-Bab

# Purpose

This directory contains folders needed to run example_biosim.py There are 2 subdirectories
1. Data directory saves log files
2. Graphics directory saves

The two subdirectories only contain README.mds, since graphics and logfiles are not to be committed 
to git repository (only kept in local branch). 