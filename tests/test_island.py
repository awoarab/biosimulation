__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

"""
Testing of Island class
"""

import pytest
import textwrap
from biosim.island import Island
from biosim.landscape import Water, Lowland
from biosim.animals import Herbivore


class TestIsland:
    @pytest.fixture(autouse=True)
    def reset_island(self):
        """Fixture that creates an empty island, and resets Herbivore parameters"""
        geogr = """\
                    WWWWW
                    WLLLW
                    WLLLW
                    WLLLW
                    WWWWW"""

        geogr = textwrap.dedent(geogr)
        self.island = Island(map_string=geogr)
        yield
        Herbivore.set_params(Herbivore.default_parameters)

    @pytest.mark.parametrize('symbol', ['A', 'w', 'd', 'B'])
    def test_correct_params(self, symbol):
        """Tests that KeyError is raised in invalid letters are given in island geography"""
        with pytest.raises(ValueError):
            Island(map_string=f"W{symbol}WW\nWLHW\nWWDW\nWWWW")

    @pytest.mark.parametrize('maps',
                             ["WWW\nWLHW\nWWDW\nWWWW",
                              "WWWW\nWHW\nWWDW\nWWWW"])
    def test_correct_dims(self, maps):
        """Tests that the ValueError is raised if rows do not have same length"""
        with pytest.raises(ValueError):
            Island(map_string=maps)

    @pytest.mark.parametrize('maps',
                             ["LWWW\nWLHW\nWWDW\nWWWW",
                              "WWWW\nWHWW\nWWDL\nWWWW"])
    def test_bad_boundary_col(self, maps):
        """Tests that ValueError is raised if not first and last column is water"""
        with pytest.raises(ValueError):
            Island(map_string=maps)

    @pytest.mark.parametrize('maps',
                             ["WLWW\nWLHW\nWWDW\nWWWW",
                              "WWWW\nWHWW\nWWDW\nWWLW"])
    def test_bad_boundary_row(self, maps):
        """Tests that ValueError is raised if not first and last row is water"""
        with pytest.raises(ValueError):
            Island(map_string=maps)

    def test_create_full_map_dict_locations(self):
        """Test that number of cells (locations) based on the provided geography is 25"""
        assert len(self.island.full_map.keys()) == 25

    def test_create_full_map_dict_object(self):
        """Test that full_map includes water as well as dryland """
        for _, value in self.island.full_map.items():
            assert type(value) is Water or type(value) is Lowland

    def test_filter_water(self):
        """Test that water is filtered out"""
        for coor in self.island.dryland_coors:
            assert type(self.island.full_map[coor]) is not Water

    @pytest.mark.parametrize('ini_pop',
                             [([{'place': (2, 3),
                                 'pop': [{'species': 'Herbivore', 'age': 1, 'weight': 10}]}]),
                              ([{'loc': (2, 3),
                                 'animals': [{'species': 'Herbivore', 'age': 1, 'weight': 10}]}])])
    def test_populate_cell_keyerror(self, ini_pop):
        """Test that Key error is raised when dictionary key is not correct"""
        with pytest.raises(KeyError):
            self.island.populate_cell(ini_pop)

    @pytest.mark.parametrize('water_cells_loc',
                             [(1, 1), (2, 1), (25, 25)])
    def test_habitable_populate_cell(self, water_cells_loc):
        """Test if Value Error is raised for uninhabited cells"""
        ini_pop = [{'loc': water_cells_loc,  # water cell
                    'pop': [{'species': 'Herbivore', 'age': i, 'weight': 100}
                            for i in range(10)]}]
        with pytest.raises(ValueError):
            self.island.populate_cell(ini_pop)

    def test_populate_cell(self):
        """Test that the expected no of animals are placed in the correct location"""
        n_herb = 10
        n_carn = 15
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': i, 'weight': 100}
                            for i in range(n_herb)]},
                   {'loc': (2, 3),
                    'pop': [{'species': 'Carnivore', 'age': i, 'weight': 100}
                            for i in range(n_carn)]}]
        self.island.populate_cell(ini_pop)
        cell_herb = self.island.full_map[(3, 3)]
        cell_carn = self.island.full_map[(2, 3)]
        assert len(cell_herb.herbivores_list) == n_herb
        assert len(cell_carn.carnivores_list) == n_carn

    def test_count_herbivores(self):
        """Test that both cellwise counts and total counts returned are as expected"""
        n_herb = 10
        n_carn = 15
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': i, 'weight': 100}
                            for i in range(n_herb)]},
                   {'loc': (2, 3),
                    'pop': [{'species': 'Carnivore', 'age': i, 'weight': 100}
                            for i in range(n_carn)]}]
        self.island.populate_cell(ini_pop)
        herb_total, herb_cellwise = self.island.count_herbivores()
        assert herb_total == n_herb
        assert herb_cellwise[3, 3] == n_herb

    def test_count_carnivores(self):
        """Test that both cellwise counts and total counts returned are as expected"""
        n_herb = 10
        n_carn = 15
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': i, 'weight': 100}
                            for i in range(n_herb)]},
                   {'loc': (2, 3),
                    'pop': [{'species': 'Carnivore', 'age': i, 'weight': 100}
                            for i in range(n_carn)]}]
        self.island.populate_cell(ini_pop)
        carn_total, carn_cellwise = self.island.count_carnivores()
        assert carn_total == n_carn
        assert carn_cellwise[2, 3] == n_carn

    def test_allow_migration_check_dict(self):
        """Test that when migration is likely, the seasonal_migrants dict is filled"""
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': 1, 'weight': 5000}
                            for _ in range(100)]}]
        self.island.populate_cell(ini_pop)
        Herbivore.set_params({'mu': 1.0})
        self.island.allow_migration()
        migrants = 0
        for migrants_list in self.island.seasonal_migrants.values():
            migrants += len(migrants_list)
        assert migrants > 0

    def test_allow_migration_non_migrants(self):
        """Test that when migration is less likely that there are remaining animals in the cell"""
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': 40, 'weight': 10}
                            for _ in range(100)]}]
        self.island.populate_cell(ini_pop)
        Herbivore.set_params({'mu': 0.05})
        self.island.allow_migration()
        n_remaining = len(self.island.full_map[3, 3].herbivores_list)
        assert n_remaining > 0

    def test_distribute_seasonal_migrants(self):
        """Test that when migration is likely, distribute_seasonal_migrants method
        assigns animals to neighbouring cells"""
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': 1, 'weight': 5000}
                            for _ in range(1000)]}]
        self.island.populate_cell(ini_pop)
        Herbivore.set_params({'mu': 1.0})
        self.island.allow_migration()
        self.island.distribute_seasonal_migrants()
        adjacent_cells = [(2, 3), (4, 3), (3, 2), (3, 4)]
        for coor in adjacent_cells:
            n_animals = len(self.island.full_map[coor].herbivores_list)
            assert n_animals > 0

    def test_seasonal_update_empty_dict(self):
        """Assume that migration is likely and that seasonal_migrants dict has been filed.
        Test that seasonal_migrant dict is emptied once seasonal_update_island is run"""
        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore', 'age': 1, 'weight': 5000}
                            for _ in range(100)]}]
        self.island.populate_cell(ini_pop)
        Herbivore.set_params({'mu': 1.0})
        self.island.allow_migration()
        self.island.seasonal_update_island()
        assert len(self.island.seasonal_migrants.items()) == 0
