__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

"""
Testing of the general Animal class (using Herbivores subclass)
and methods that are specific to the Carnivores subclass
"""

import random
import pytest
from biosim.animals import Herbivore
from biosim.animals import Carnivore
import scipy.stats as stats
import math

SEED = 1212
ALPHA = 0.01


class TestAnimal:
    @pytest.fixture
    # inspired by test_bacteria.py:
    def reset_herbivore_defaults(self):
        """Teardown of fixture. Resets the default parameters in herbivore instances after a test
        Resets also the herbivore instance used for testing (ex. if age or weight is changed)"""
        # no setup
        yield
        Herbivore.set_params(Herbivore.default_parameters)

    @pytest.fixture(autouse=True)
    def create_herbivore(self):
        """Creating fixture. Creates a herbivore with age = 10 and weight = 10.
        Set to be used for all testing in TestAnimal, but may not be implemented in certain tests"""
        self.herbivore = Herbivore(10, 10)
        self.herbivore.update_fitness()

    @pytest.mark.parametrize('new_param',
                             [({"W_birth": 10, 'Sigma_birth': 5}),
                              ({"a_haLf": 20})])
    def test_set_params_keyerror(self, new_param):
        """Tests that KeyError is raised if wrong keys are passed to Animal-object"""
        # herbivore = Herbivore(10, 10)
        with pytest.raises(KeyError):
            self.herbivore.set_params(new_param)

    @pytest.mark.parametrize('new_param',
                             [({'w_birth': -1, 'DeltaPhiMax': 1, 'eta': 0.05}),
                              ({'w_birth': 8.0, 'DeltaPhiMax': 0, 'eta': 0.05}),
                              ({'w_birth': 8.0, 'DeltaPhiMax': 1, 'eta': 1.2})])
    def test_set_params_valueerror(self, new_param):
        """Tests all cases of invalid parameters passed to Animal-object"""
        with pytest.raises(ValueError):
            self.herbivore.set_params(new_param)

    @pytest.mark.parametrize('new_params, key, expected',
                             [({'w_birth': 10}, 'w_birth', 10),
                              ({'sigma_birth': 0.5}, 'sigma_birth', 0.5),
                              ({'F': 110}, 'F', 110)])
    def test_update_params(self, reset_herbivore_defaults, new_params, key, expected):
        """Tests that set_params updates the parameters in the Animal object correctly"""
        self.herbivore.set_params(new_params)
        assert self.herbivore.current_parameters[key] == expected

    @pytest.mark.parametrize('rel_params, expected',
                             [(None, Herbivore.current_parameters),
                              ('w_birth', 8)])
    def test_get_params(self, rel_params, expected):
        """
        Test that get_params returns:
            * All current parameters if no param_key argument is not specified
            * Expected parameter value is returned if param_key is specified
        """
        assert self.herbivore.get_params(rel_params) == expected

    @pytest.mark.parametrize('age, weight',
                             [(1, 2),
                              (10, 10),
                              (17, 200)])
    def test_animal_attributes(self, age, weight):
        """Test that instances are initialised with the correct attribute values"""
        herbivore = Herbivore(age, weight)

        assert herbivore.age == age
        assert herbivore.weight == weight

    @pytest.mark.parametrize('age, weight, fitness',
                             [(1, 2, 0.31002551885),
                              (10, 10, 0.4999999923850),
                              (17, 200, 0.99999897877)],
                             )
    def test_fitness_attribute(self, age, weight, fitness):
        """
        Tests that we:
            1. Fitness is set on the creation of an animal instance
            2. Fitness is calculated according to expectations
        """
        herbivore = Herbivore(age, weight)
        herbivore.update_fitness()
        assert herbivore.fitness == pytest.approx(fitness)

    def test_fitness_zero_weigth(self):
        """Test that fitness is set to 0 when animal weight is 0"""
        for age in range(0, 10):
            herbivore = Herbivore(age, 0.5)
            herbivore.weight = 0
            herbivore.update_fitness()
            assert herbivore.fitness == 0

    def test_certain_death(self):
        """Test that an animal dies for certain when weight is 0"""
        herbivore = Herbivore(10, 10)
        herbivore.weight = 0  # simulating loss of weight
        herbivore.update_fitness()
        assert herbivore._death_function()

    @pytest.mark.parametrize('age, weight', [(i, i * 2) for i in range(1, 51)])
    def test_death_z_test(self, age, weight):
        """Statistical test of the death_function"""
        random.seed(SEED)
        # Test based on test_bacteria.py
        herbivore = Herbivore(age, weight)
        herbivore.update_fitness()
        num_trials = 100
        # Retrieving parameter needed to find prob_death
        omega = herbivore.get_params(param_key='omega')
        curr_fitness = herbivore.fitness
        prob_death = omega * (1 - curr_fitness)
        num_deaths = sum(herbivore._death_function() for _ in range(num_trials))
        # Reference test_bacteria.py
        mean = num_trials * prob_death
        var = num_trials * prob_death * (1 - prob_death)
        z_score = (num_deaths - mean) / math.sqrt(var)
        phi = 2 * stats.norm.cdf(-abs(z_score))
        assert phi > ALPHA

    @pytest.mark.parametrize('age, weight',
                             [(i, i * 5) for i in range(1, 20)])
    def test_reproduce_no_birth_zeta(self, reset_herbivore_defaults, age, weight):
        """Test that first condition for parent is satisfied prior to reproduction.
        Condition tested is:
            * No birth occurs when weight parent < zeta*(w_birth+sigma_birth).
        Parameter xi  set to 0 to avoid weight > newborn*xi being violated
        Parameter zeta set high to make violation of tested condition likely"""
        N = 10
        herbivore = Herbivore(age, weight)
        herbivore.set_params({'zeta': 100, 'xi': 0})
        herbivore.update_fitness()
        assert herbivore.reproduce(N) is None

    @pytest.mark.parametrize('age, weight',
                             [(i, i * 5) for i in range(1, 20)])
    def test_reproduce_no_birth_xi(self, reset_herbivore_defaults, age, weight):
        """Test that second condition for parent is satisfied prior to reproduction.
        Condition tested is:
         * No birth occurs when weight parent <= xi*newborn_weight
        Parameter zeta is set to 0 to avoid violating weight >= zeta*(w_birth+sigma_birth)
        Parameter xi set high to make violation of tested condition likely"""
        N = 10
        herbivore = Herbivore(age, weight)
        herbivore.set_params({'xi': 100, 'zeta': 0})
        herbivore.update_fitness()
        assert herbivore.reproduce(N) is None

    @pytest.mark.parametrize('age, weight', [(i, i * 2) for i in range(1, 21)])
    def test_reproduction_z_test(self, reset_herbivore_defaults, age, weight):
        """Statistical test for reproduction function"""
        random.seed(SEED)
        # Test based on test_bacteria.py
        herbivore = Herbivore(age, weight)
        herbivore.update_fitness()
        herbivore.set_params({'zeta': 0, 'xi': 0})  # Ensure that conditions of birth always met
        num_trials = 100
        N = 2
        # Retrieving parameter needed to find prob_death
        gamma = herbivore.get_params('gamma')
        fitness = herbivore.fitness
        prob_birth = min(1, N * gamma * fitness)
        num_newborns = sum([herbivore.reproduce(N) is not None for _ in range(num_trials)])
        # Reference test_bacteria.py
        mean = num_trials * prob_birth
        var = num_trials * prob_birth * (1 - prob_birth)
        z_score = (num_newborns - mean) / math.sqrt(var)
        phi = 2 * stats.norm.cdf(-abs(z_score))
        assert phi > ALPHA

    @pytest.mark.parametrize('fodder_received', [i for i in range(10)])
    def test_eat_fodder_less_than_f(self, reset_herbivore_defaults, fodder_received):
        """Test that when fodder received is less than F (10 by default), the weight increases only
        by fodder_received * beta. Keep in mind that self.herbivore has weight = 10"""
        weight = self.herbivore.weight
        beta = self.herbivore.get_params('beta')
        self.herbivore.eat_fodder(fodder_received)
        expected_weight = weight + fodder_received * beta
        assert self.herbivore.weight == expected_weight

    @pytest.mark.parametrize('fodder_received', [i for i in range(11, 101)])
    def test_eat_fodder_more_than_f(self, reset_herbivore_defaults, fodder_received):
        """Test that when fodder received is greater than F (10 by default), the weight
        increases only by F * beta"""
        weight = self.herbivore.weight
        beta = self.herbivore.get_params('beta')
        F = self.herbivore.get_params('F')
        self.herbivore.eat_fodder(fodder_received)
        expected_weight = weight + F * beta
        assert self.herbivore.weight == expected_weight

    @pytest.mark.parametrize('weight', [(i * 1000) for i in range(1, 6)])
    def test_will_migrate_certain(self, weight, reset_herbivore_defaults):
        """Test that animals always migrate of mu*fitness is large"""
        self.herbivore.set_params({'mu': 1})
        self.herbivore.weight = weight
        assert self.herbivore.will_migrate() is True

    @pytest.mark.parametrize('age, weight', [(i, i) for i in range(1, 100)])
    def test_will_migrate_z_test(self, age, weight):
        """Statistical test for the will_migrate function"""
        random.seed(SEED)
        herbivore = Herbivore(age, weight)
        herbivore.update_fitness()
        num_trials = 100
        # Retrieving parameter needed to find prob_death
        mu = herbivore.get_params(param_key='mu')
        curr_fitness = herbivore.fitness
        prob_mig = mu * curr_fitness
        num_mig = sum([herbivore.will_migrate() for _ in range(num_trials)])
        # Reference test_bacteria.py
        mean = num_trials * prob_mig
        var = num_trials * prob_mig * (1 - prob_mig)
        z_score = (num_mig - mean) / math.sqrt(var)
        phi = 2 * stats.norm.cdf(-abs(z_score))
        assert phi > ALPHA

    @pytest.mark.parametrize('age', [i for i in range(100)])
    def test_seasonal_update_age(self, reset_herbivore_defaults, age):
        """Test that the age of an animal is updated for each season"""
        self.herbivore.age = age
        self.herbivore.seasonal_update_animal()
        assert self.herbivore.age == age + 1

    @pytest.mark.parametrize('weight', [i for i in range(100)])
    def test_seasonal_update_weight(self, reset_herbivore_defaults, weight):
        """Test that the weight of an animal is updated each season, and that weight is according
        to expectation (weight should decrease by eta*weight)"""
        self.herbivore.weight = weight
        eta = self.herbivore.get_params('eta')
        self.herbivore.seasonal_update_animal()
        assert self.herbivore.weight == weight - weight * eta


class TestCarnivore:
    @pytest.fixture
    # inspired by test_bacteria.py:
    def reset_carnivore_defaults(self):
        """Teardown of fixture. Resets the default parameters in carnivore instances after a test
        Resets also the carnivore instance used for testing (ex. if age or weight is changed)"""
        # no setup
        yield
        Carnivore.set_params(Carnivore.default_parameters)
        self.carnivore = Carnivore(1, 50)
        self.carnivore.update_fitness()

    @pytest.fixture(autouse=True)
    def create_carnivore(self):
        """Creating fixture. Creates a carnivore with age = 10 and weight = 10.
        Set to be used for all testing in TestCarnivore , but may not be implemented in
        certain tests"""
        self.carnivore = Carnivore(1, 50)
        self.carnivore.update_fitness()

    def test_will_kill_certain(self, reset_carnivore_defaults):
        """Check that when the else condition is True (fitness difference between carnivore and
        herbivore exceed DeltaPhiMax) then the herbivore is always killed. Set DeltaPhiMax low
        and carnivore fitness high so that the condition is always True """
        self.carnivore.fitness = 1
        # Setting DeltaPhiMax low so that the difference between the fittest herbivore and
        # carnivore with fitness = 1 is less than 0.005. That will yield the else condition
        self.carnivore.set_params({'DeltaPhiMax': 0.005})
        herbivores_list = [Herbivore(i, i * 2) for i in range(1, 100)]
        [herbivore.update_fitness() for herbivore in herbivores_list]
        number_killed = sum([self.carnivore._will_kill(herbivore)
                             for herbivore in herbivores_list])
        assert number_killed == len(herbivores_list)

    @pytest.mark.parametrize('age, weight', [(i, i * 2) for i in range(1, 21)])
    def test_will_kill_z_test(self, reset_carnivore_defaults, age, weight):
        """Checks that the expected number of herbivores are killed"""
        random.seed(SEED)
        DeltaPhiMax = self.carnivore.get_params('DeltaPhiMax')
        num_trials = 100
        herbivores_list = [Herbivore(age, weight) for _ in range(num_trials)]
        [herbivore.update_fitness() for herbivore in herbivores_list]
        prob_kill = (self.carnivore.fitness - herbivores_list[0].fitness) / DeltaPhiMax
        num_kills = sum([self.carnivore._will_kill(herbivore)
                         for herbivore in herbivores_list])
        # Reference test_bacteria.py
        mean = num_trials * prob_kill
        var = num_trials * prob_kill * (1 - prob_kill)
        z_score = (num_kills - mean) / math.sqrt(var)
        phi = 2 * stats.norm.cdf(-abs(z_score))
        assert phi > ALPHA

    @pytest.mark.parametrize('age, weight', [(i, i * 2) for i in range(1, 21)])
    def test_no_herbivores_eaten(self, reset_carnivore_defaults, age, weight):
        """Test that no herbivores are eaten when fitness of carnivore is much that of
        herbivores"""
        self.carnivore.fitness = 0.005
        herbivores_list = [Herbivore(age, weight) for _ in range(100)]
        [herbivore.update_fitness() for herbivore in herbivores_list]
        herbivores_list.sort(key=lambda item: item.fitness, reverse=True)
        number_remaining = len(self.carnivore.hunt_and_feed(herbivores_list))
        assert len(herbivores_list) == number_remaining

    @pytest.mark.parametrize('age, weight', [(i, i * 2) for i in range(1, 21)])
    def test_some_herbivores_eaten(self, reset_carnivore_defaults, age, weight):
        """Test that some or no herbivores are eaten with the hunt_and_feed method"""
        herbivores_list = [Herbivore(age, weight) for _ in range(100)]
        [herbivore.update_fitness() for herbivore in herbivores_list]
        herbivores_list.sort(key=lambda item: item.fitness, reverse=True)
        number_remaining = len(self.carnivore.hunt_and_feed(herbivores_list))
        assert len(herbivores_list) >= number_remaining
