__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'
"""
Testing of Landscape superclass (using a Lowland subclass) and Water class
"""

import pytest
from biosim.landscape import Lowland, Water
from biosim.animals import Herbivore
from biosim.animals import Carnivore


class TestLowland:
    """
    Test class used to test all the methods in:
        * Landscape-superclass
        * Methods specific to Lowland
    Lowland and Highland share methods, hence we are checking Highland too
    """

    @pytest.fixture(autouse=True)
    def reset_parameters_default(self):
        """Test fixture ran for every function defined in TestLowland. Creates a new instance of
        lowland cell and resets landscape parameters to default"""
        self.lowland_cell = Lowland(2, 2)
        yield
        Lowland.set_params(Lowland.default_parameters)

    def test_set_params(self):
        """Tests if we can set default parameters ourselves"""
        f_set = 10
        self.lowland_cell.set_params({'f_max': f_set})
        assert self.lowland_cell.get_params('f_max') == f_set

    @pytest.mark.parametrize('new_param',
                             [({'F_max': 10}), ({'not_param': 10}),
                              ({'f_max': 10, 'not_param': 10})])
    def test_set_params_keyerror(self, new_param):
        """Test KeyError handling (only valid key is f_max)"""
        with pytest.raises(KeyError):
            self.lowland_cell.set_params(new_param)

    @pytest.mark.parametrize('new_param',
                             [({'f_max': -i}) for i in range(1, 10)])
    def test_set_params_valueerror(self, new_param):
        """Test ValueError handling (f_max >= 0)"""
        with pytest.raises(ValueError):
            self.lowland_cell.set_params(new_param)

    def test_add_animal_number(self):
        """Number of animals added meets expected number of animals.
        Checks that len(lowland.herbivore_list) is of expected size"""
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Herbivore', 'age': 3, 'weight': 30}]
        expected_number = 3
        self.lowland_cell.add_animals(ini_pop)
        assert len(self.lowland_cell.herbivores_list) == expected_number

    def test_add_animal_correct_list(self):
        """Test that correct species is added to correct list.
        Checks that all instances in lowland.herbivore_list is Herbivore subclass"""
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Carnivore', 'age': 3, 'weight': 30}]
        self.lowland_cell.add_animals(ini_pop)
        assert all([type(herbivore) is Herbivore
                    for herbivore in self.lowland_cell.herbivores_list])
        assert all([type(carnivore) is Carnivore
                    for carnivore in self.lowland_cell.carnivores_list])

    def test_add_animal_keyerror(self):
        """Check that KeyError is raised as expected with this test.
        Parameterize test (run for different wrong keys)"""
        ini_pop = [{'animal': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Herbivore', 'age': 3, 'weight': 30}]
        with pytest.raises(KeyError):
            self.lowland_cell.add_animals(ini_pop)

    def test_add_animal_valueerror(self):
        """Check that Value is raised as expected with this test.
        Parameterize test (run for different wrong values for species)"""
        ini_pop = [{'species': 'animal', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Herbivore', 'age': 3, 'weight': 30}]
        with pytest.raises(ValueError):
            self.lowland_cell.add_animals(ini_pop)

    def test_feed_herbivore_weight_increase(self):
        """
        Assume that we have abundant fodder in the cell. Check that:
        * Total weight of herbivores increases when we feed them
        * Check that sum of weights is as expected (remember that new_weight = weight + F*beta)
        * That fodder_available attribute decreases
        """
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Herbivore', 'age': 3, 'weight': 30}]
        initial_weight = 60
        self.lowland_cell.add_animals(ini_pop)
        self.lowland_cell.feed_herbivores()
        total_weight = sum([herbivore.weight for herbivore in self.lowland_cell.herbivores_list])
        assert total_weight > initial_weight

    def test_feed_herbivore_f_decrease(self):
        """
        Assume that we have abundant fodder in the cell. Check that:
            * Total weight of herbivores increases when we feed them
            * Check that sum of weights is as expected (remember that new_weight = weight + F*beta)
            * That fodder_available attribute decreases
        """
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Herbivore', 'age': 3, 'weight': 30}]
        f_max = self.lowland_cell.get_params('f_max')  # getting para from landscape
        self.lowland_cell.add_animals(ini_pop)
        self.lowland_cell.feed_herbivores()
        assert self.lowland_cell.fodder_available < f_max

    def test_feed_carnivores(self):
        """
        Tests that once we feed carnivores, the number of herbivores in the cell
        decrease from the original number of herbivores
        """
        ini_herbs = [{'species': 'Herbivore', 'age': i, 'weight': i * 2}
                     for i in range(1, 50)]
        ini_carnivores = [{'species': 'Carnivore', 'age': i, 'weight': i * 2}
                          for i in range(1, 50)]
        self.lowland_cell.add_animals(ini_herbs)
        self.lowland_cell.add_animals(ini_carnivores)
        self.lowland_cell.feed_carnivores()
        assert len(ini_herbs) >= len(self.lowland_cell.herbivores_list)

    def test_feed_carnivores_weight_increase(self):
        """Assume that we have abundant fodder in the cell. Check that:
             * Total weight of carnivore increases when we feed them"""
        ini_carns = [{'species': 'Carnivore', 'age': 1, 'weight': 10},
                     {'species': 'Carnivore', 'age': 2, 'weight': 20},
                     {'species': 'Carnivore', 'age': 3, 'weight': 30}]
        initial_weight = 60
        self.lowland_cell.add_animals(ini_carns)
        ini_herbs = [{'species': 'Herbivore', 'age': 1, 'weight': 2} for _ in range(50)]
        self.lowland_cell.add_animals(ini_herbs)
        self.lowland_cell.feed_carnivores()
        total_weight = sum([carnivore.weight for carnivore in self.lowland_cell.carnivores_list])
        assert total_weight >= initial_weight

    def test_reproduction_when_likely(self):
        """Create herbivores with large weights, and many herbivores in one cell
        (makes prob_birth large).
        Check that total number of herbivore's increase after running Lowland.allow_reproduction"""
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 500},
                   {'species': 'Herbivore', 'age': 2, 'weight': 700},
                   {'species': 'Herbivore', 'age': 3, 'weight': 1000}]
        initial_number = 3
        self.lowland_cell.add_animals(ini_pop)
        self.lowland_cell.allow_reproduction()
        total_animal = len(self.lowland_cell.herbivores_list)
        assert total_animal >= initial_number

    def test_allow_reproduction_nonetype(self):
        """Run Lowland.allow_reproduction for herbivores with low weights (and few herbivores
         in one cell).This makes prob_birth low and will result in few animals giving birth.
         The Animal.reproduce will return many None-values.
        GOAL: Check that allow_reproduction removes all NoneTypes from Lowland.herbivore_list"""
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 50},
                   {'species': 'Herbivore', 'age': 2, 'weight': 70},
                   {'species': 'Herbivore', 'age': 3, 'weight': 100}]
        self.lowland_cell.add_animals(ini_pop)
        self.lowland_cell.allow_reproduction()
        assert all(self.lowland_cell.herbivores_list) is not None

    @pytest.mark.parametrize('Herbivores, Carnivores',
                             [([Herbivore(10, 10) for i in range(50)],
                               [Carnivore(10, 10) for i in range(50)])])
    def test_add_migrants_correct_list(self, Herbivores, Carnivores):
        """Test that correct species is added to correct list.
        Checks that all instances in lowland.herbivore_list is Herbivore subclass"""
        self.lowland_cell.add_migrants(Herbivores)
        self.lowland_cell.add_migrants(Carnivores)
        assert all([type(herbivore) is Herbivore
                    for herbivore in self.lowland_cell.herbivores_list])
        assert all([type(carnivore) is Carnivore
                    for carnivore in self.lowland_cell.carnivores_list])

    def test_help_create_migrants(self):
        """Test that potential migrants list is created using the _create_migrants"""
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 100 * i} for i in range(1, 50)]
        self.lowland_cell.add_animals(ini_pop)
        pot_migrants, _ = self.lowland_cell._create_migrants(self.lowland_cell.herbivores_list)
        assert len(pot_migrants) > 0

    def test_seasonal_update_landscape_regrowth(self):
        """Test that fodder regrows"""
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Carnivore', 'age': 3, 'weight': 30}]
        f_max = self.lowland_cell.get_params('f_max')  # getting para from landscape
        self.lowland_cell.add_animals(ini_pop)
        self.lowland_cell.feed_herbivores()
        self.lowland_cell.seasonal_update_landscape()
        assert self.lowland_cell.fodder_available == f_max


class TestWater:
    """Test class used for testing methods specific to Water class"""

    def test_add_animals_valueerror(self):
        water_cell = Water(12, 12)
        ini_pop = [{'species': 'Herbivore', 'age': 1, 'weight': 10},
                   {'species': 'Herbivore', 'age': 2, 'weight': 20},
                   {'species': 'Carnivore', 'age': 3, 'weight': 30}]
        with pytest.raises(TypeError):
            water_cell.add_animals(ini_pop)
