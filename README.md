# INF 200 BioSim projects January 2023 

# Introduction
In this project we have modelled the Ecosystem of Rossumøya by developing computer programs for the simulation of population dynamics, called BioSim. As we know, an island consists of water, desert, highland, and lowland, and the species on the island are Herbivore and Carnivore which can only be found on dry-land (desert, highland, and lowland). Furthermore, animals have certain characteristics, among other are that they age, reproduce, die, migrate, etc. For this project, all the characteristics specified in the “Modelling the Ecosystem of Rossumøya Project description” has been considered and python functions have been created accordingly.

**An example file is located in examples/example_biosim.py**. Results (imgage files, mp4-movie and log-files) are stored in the resutls directory. **Note** that results and its subdirectories have gitignore-files (so that png, mp4 and csv are not committed to the repository).

# Python files 
For this project we focused on three core BioSim files, namely animal.py, landscape.py, and island.py. These files are the base for the animal and island dynamics, and in each of these files contains a class and different methods that have been coded to cover these dynamics. Additionally, animal.py and landscape.py contains subclasses. The files, especially the methods are provided with docstrings explaining what they do, the parameters and the output. In addition to the core files, we also have simulation and visualisation files, simulation.py and graphics.py where simulation takes in the methods from the other files and simulated the population dynamics whereas graphics is for visualising the different graphs. Thus, the most significant files for the user to modelling the ecosystem is simulation.py. 

For each of the three python files, test files have also been created with the names test_animal.py, test_landscape.py, and test_island.py. These test files contains methods that  checks whether the methods in the main python files deliver correct output. Thus, these tests have been coded and ran simultaneously while writing the main python files, and all tests have passed. Docstrings are also provided for these test methods where a brief explanation is given. 


## BioSim Class (simulation.py)
The BioSim class is the top-level interface to BioSim package. It Implements and controls simulations for biosim-package, and it is what the user runs to get the ecosystem required. Thus, simply put, it contains methods that ‘calls in’ the methods created in the core python files. 

## Graphics Class (graphics.py)
The Graphics class is for visualising our ecosystem. It visualises an island map, animal count, herbivore distribution, carnivore distribution, and fitness, age and weight for carnivores and herbivores. All visualisations apart from island map are of movies format, meaning that an MPEG4 movie have been created from the visualisation images saved. 

## Animal Class (animal.py)
Animal class is the superclass/main class that manages two subclasses:
- Herbivores
- Carnivores
Since the two animal species do many of the same things, the Animal class manages attributes and methods (functions) for both species.

### Attributes
Every instance of Animal has the following attributes 
- age: The age of an animal cannot be negative (and can only increase). 
- weight: The weight of an animal cannot be less than 0 on creation (death if weight is 0)
- fitness: Depends on only age and weight. Calculated on creation of an instance (currently)

In addition, every subclass of Animal has the following class attributes
- default_parameters:  parameters unique for subclasses Herbivore and Carnivore. They are given in the task description (see table 2). They should not be changed, since we may want to revert to them.
- current_parameters: copy of default_parameters. This may be changed, and this is what the methods set_params and get_params manipulate/extract. This is also the dictionary to be used in methods that need parameters. 

### Methods
Methods in Animal class include class methods, static methods and normal methods. Class methods include set_params and get_params functions. 

Class methods are used to update/get parameter for all instances of a given subclasses (example change w_birth parameter for all Herbivore instances). 

Static methods are used to create mathematical functions that take in input-variables and spit out output (think of it as normal f(x) = y functions, where self-argument is not needed). _q(x,x_half,phi) is such a method.  

## Landscape Class (landscape.py)
Like the Animal class, the Landscape superclass uses class methods, static methods and normal methods. The landscape class is used to keep and manage animals in one given cell, and manages four subclasses:
- Highland
- Lowland
- Desert
- Water

### Attributes 
Every landscape instance has the following attributes:
- x: x-coordinate of its location 
- y: y-coordinate of its location
- herbivores_list: List of objects that are of subclass Herbivores
- carnivores_list: List of objects that are of subclass Carnivores

Within each subclass, the following class attributes are shared between instances of the same subclass:
- fodder_available: Fodder available in the cell at the time is updated when herbivores eat. 

### Methods
Like in the Animal class we have static methods and class methods where the class methods are set_params and get_params, and they work in the same manner. They are therefore not described again here.


## Island Class (island.py)
Similar to both the Animal and Landscape classes, the Island superclass uses class methods, static methods and normal methods. The Island class is used to create the map and manage migrations. 

### Attributes 
Every island instance has the following attributes:
- map_string: Multiline string specifying the island's geography

### Methods
Similar to the Animal and Landscape classes we have static methods and class methods where the class methods, and they work in the same manner. They are therefore not described here.






