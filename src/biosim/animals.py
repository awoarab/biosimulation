# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2023 Awo Arab & Alin Dak Al / NMBU
__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

import random
from math import exp, log, sqrt


class Animal:
    """Animal superclass"""

    @classmethod
    # Class method based on examples in lecture notes (bacteria.py)
    def set_params(cls, new_params):
        """
        Check whether the keys in the parameter dictionary inputted by the user
        is the same as those in the current_parameters dictionary.

        Parameters
        ----------
        new_params: dict
            Dictionary, parameters for animal type.

        Raises
        -------
        KeyError
            If key is not defined in default_params.
        ValueError
            If key does not meet criteria (see table 2 in project description).

        Notes
        ------

        .. code::

            Herbivore.set_params({'w_birth':10, 'sigma_birth':20})

        """
        for key in new_params:
            # noinspection PyUnresolvedReferences
            if key not in list(cls.current_parameters.keys()):
                raise KeyError('Invalid parameter name: ' + key)
            elif new_params[key] < 0:
                raise ValueError('Parameter must be equal to or greater than 0 ' + key)
            elif key == 'DeltaPhiMax' and new_params[key] <= 0:
                raise ValueError('Parameter must be strictly greater than 0 ' + key)
            elif key == 'eta' and new_params[key] > 1:
                raise ValueError('Parameter must be equal less or equal to 1 ' + key)
            else:
                # noinspection PyUnresolvedReferences
                cls.current_parameters[key] = new_params[key]

    @classmethod
    # Class method based on examples in lecture notes (bacteria.py)
    def get_params(cls, param_key=None):
        """
        Gets the parameters entered by the user.

        Parameters
        ----------
        param_key: str
            Key corresponding to an animal parameter. See Table 2 for parameter naming

        Returns
        -------
        current_parameters: dict
            Dictionary with current parameters for a subclass if param_key = None
        current_parameters: float
            Single float value if param_key is specified

        Notes
        ------

        .. code:: python

            Herbivore.get_params({'w_birth':10, 'sigma_birth':20})
            Carnivore.get_params()

        """
        if param_key is None:
            # noinspection PyUnresolvedReferences
            return cls.current_parameters
        else:
            # noinspection PyUnresolvedReferences
            return cls.current_parameters[param_key]

    def __init__(self, age, weight):
        """
        Parameters
        ----------
        age: int
            The age of the animal
        weight: float
            The weight of the animal

        Raises
        ------
        ValueError:
            Raised if age is set to a negative number, or if weight is set to less than or to 0
        """
        if age < 0:
            raise ValueError("Age cannot be negative")
        elif weight <= 0:
            raise ValueError("Weight must be greater than zero")
        else:
            self.age = age
            self.weight = weight
            # Fitness updated in other methods
            self.fitness = None

    @staticmethod
    def _q_equation(x, x_half, phi):
        """
        Helper function used to calculate fitness. Input depends on the animal's current
        age/weight and current parameters. Function corresponds to equation no. 4

        Parameters
        ----------
        x: float
            Either current age or weight
        x_half: float
            Either age_half or weight_half
        phi: float
            Either phi_age or phi_weight
        """
        return 1 / (1 + exp(phi * (x - x_half)))

    def update_fitness(self):
        """
        Function updates the Animal object's fitness if called on.

        Notes
        -----

        Uses helper function _q_equation which is

        .. math::
            q^{\\pm} = \\frac{1}{1 + e ^{\\pm\\phi(x-x_{1/2})}}

        to produce fitness function

        .. math::
            \\Phi  = \\frac{1}{1 + e ^{+\\phi(a-a_{1/2})}} \\
             \\times \\frac{1}{1 + e ^{-\\phi(w-w_{1/2})}}

        """
        # noinspection PyUnresolvedReferences
        a_half = self.current_parameters["a_half"]
        # noinspection PyUnresolvedReferences
        w_half = self.current_parameters["w_half"]
        # noinspection PyUnresolvedReferences
        phi_age = self.current_parameters["phi_age"]
        # noinspection PyUnresolvedReferences
        phi_weight = self.current_parameters["phi_weight"]
        if self.weight <= 0:
            fitness = 0
        else:
            fitness = self._q_equation(self.age, a_half, phi_age) * \
                      self._q_equation(self.weight, w_half, -1 * phi_weight)
        self.fitness = fitness

    @staticmethod
    def _draw_newborn_weight(w, s):
        """
        A helper function that draws a number from a log-normative distribution.
        Used to simulate a newborn's weight in accordance with section 2.2.

        Parameters
        ----------
        w: float
            mean weight of newborns (assuming normal distribution).
            Corresponds to w_birth
        s: float
            standard deviation of newborn's weight (assuming normal distribution).
            Corresponds to sigma_birth

        Returns
        -------
        newborn_weight: float
            Newborn's weight
        """
        # Scales mean and mean to describe mean/std of log-normative distribution
        w_trans = log(w ** 2 / sqrt(w ** 2 + s ** 2))
        sig_trans = sqrt(log(1 + (s ** 2 / w ** 2)))
        newborn_weight = random.lognormvariate(w_trans, sig_trans)
        return newborn_weight

    # noinspection PyPep8Naming
    def reproduce(self, N):
        """
        Function used to produce new instances of animal. Function ensures that conditions for
        birth are satisfied before animal is given opportunity to reproduce (probabilistic).
        N is provided by a Landscape object.

        Parameters
        ----------
        N: int
            Number of animals of the same species in the same cell (set by Landscape-object).

        Returns
        -------
        Animal: Herbivore/Carnivore
            New instance of animal with age = 0 and weight drawn from a log-normative
            distribution. See _draw_newborn_weight
        None: NoneType
            If conditions for birth are not satisfied.

        Notes
        -----

        Uses helper function _draw_newborn_weight which has:

        .. math::
            w = log(\\frac{w^2}{\\sqrt{w^2 + s^2}})

        .. math::
            \\sigma = \\sqrt{(log(1 + \\frac{s^2}{w^2}))}
        """
        # Extracting parameters needed
        # noinspection PyUnresolvedReferences
        w_birth = self.current_parameters['w_birth']
        # noinspection PyUnresolvedReferences
        sigma_birth = self.current_parameters['sigma_birth']
        # noinspection PyUnresolvedReferences
        zeta = self.current_parameters['zeta']
        # noinspection PyUnresolvedReferences
        xi = self.current_parameters['xi']
        # noinspection PyUnresolvedReferences
        gamma = self.current_parameters['gamma']
        # Finding probability of birth
        prob_birth = min(1, gamma * self.fitness * N)
        # Drawing weight of newborn
        newborn_weight = self._draw_newborn_weight(w_birth, sigma_birth)
        if self.weight >= zeta * (w_birth + sigma_birth) and self.weight > newborn_weight * xi:
            if random.random() < prob_birth:
                self.weight -= newborn_weight * xi
                return type(self)(age=0, weight=newborn_weight)

    def eat_fodder(self, fodder_received):
        """
        The function updates an animal's weight in place and returns the amount of fodder eaten.
        Note that if no fodder is received, then else-statement is still run, but weight remains
        unchanged.

        Parameters
        ----------
        fodder_received: float
            How much fodder the animal has available. Provided by Landscape object

        Returns
        -------
        amount_eaten: float
            How much fodder the animal has eaten.
        """
        # noinspection PyUnresolvedReferences
        F = self.current_parameters['F']
        # noinspection PyUnresolvedReferences
        beta = self.current_parameters['beta']
        if fodder_received > F:
            # Update weigth and fitness
            self.weight += beta * F
            amount_eaten = F
            return amount_eaten
        else:
            self.weight += beta * fodder_received
            amount_eaten = fodder_received
            return amount_eaten

    def _death_function(self):
        """
        A helper function that updates death status of animal due to weight loss or random chance.
        Function based on equation no. 6

        Returns
        -------
        True or False: boolean type
        """
        # noinspection PyUnresolvedReferences
        omega = self.current_parameters['omega']
        prob_death = omega * (1 - self.fitness)
        r = random.random()
        if self.weight == 0 or r < prob_death:
            return True
        else:
            return False

    def will_migrate(self):
        """
        Function that identifies migration status of animal due random chance.
        Function based on description in 2.2

        Returns
        -------
        True or False: boolean type
        """
        self.update_fitness()
        # noinspection PyUnresolvedReferences
        mu = self.current_parameters['mu']
        prob_mig = mu * self.fitness
        r = random.random()
        return r < prob_mig

    def seasonal_update_animal(self):
        """
        Updates the animal's weight(loss), age increase and death status.
        Returns True if an animal is still alive (to be used by Landscape object)

        Returns
        -------
        living_status: bool
            Living status of the animal.

        Notes
        -----

        Uses helper function _death_function which has the probability of death equals

        .. math::
            \\omega(1 - \\Phi )

        """
        # noinspection PyUnresolvedReferences
        eta = self.current_parameters['eta']
        self.age += 1
        self.weight -= eta * self.weight
        self.update_fitness()
        living_status = not self._death_function()
        return living_status


class Herbivore(Animal):
    """Herbivore (subclass)"""
    default_parameters = {"w_birth": 8.0,
                          "sigma_birth": 1.5,
                          "beta": 0.9,
                          "eta": 0.05,
                          "a_half": 40.0,
                          "phi_age": 0.6,
                          "w_half": 10.0,
                          "phi_weight": 0.1,
                          "mu": 0.25,
                          "gamma": 0.2,
                          "zeta": 3.5,
                          "xi": 1.2,
                          "omega": 0.4,
                          "F": 10.0,
                          "DeltaPhiMax": 1}
    current_parameters = default_parameters.copy()

    def __init__(self, age, weight):
        """
        Parameters
        ----------
        age: int
            The age of the animal
        weight: float
            The weight of the animal
        """
        super().__init__(age, weight)


class Carnivore(Animal):
    """Carnivore (subclass)"""
    default_parameters = {"w_birth": 6.0,
                          "sigma_birth": 1.0,
                          "beta": 0.75,
                          "eta": 0.125,
                          "a_half": 40.0,
                          "phi_age": 0.3,
                          "w_half": 4.0,
                          "phi_weight": 0.4,
                          "mu": 0.4,
                          "gamma": 0.8,
                          "zeta": 3.5,
                          "xi": 1.1,
                          "omega": 0.8,
                          "F": 50.0,
                          "DeltaPhiMax": 10.0}
    current_parameters = default_parameters.copy()

    def __init__(self, age, weight):
        """
        Parameters
        ----------
        age: int
            The age of the animal
        weight: int
            The weight of the animal
        """
        super().__init__(age, weight)

    def _will_kill(self, herbivore):
        """
        A helper function used to determine if a Herbivore instance will be killed by the
        carnivore or not. Calculations based on equation no. 7

        Parameters
        ----------
        herbivore: Herbivore
            One Herbivore instance

        Returns
        -------
        True or False: boolean type
        """
        carn_fitness = self.fitness
        herb_fitness = herbivore.fitness
        # noinspection
        DeltaPhiMax = self.current_parameters['DeltaPhiMax']
        if carn_fitness <= herb_fitness:
            return False
        elif 0 < carn_fitness - herb_fitness < DeltaPhiMax:
            prob_kill = (carn_fitness - herb_fitness) / DeltaPhiMax
            return random.random() < prob_kill
        else:
            return True

    def hunt_and_feed(self, herbivore_sorted):
        """
        Function that takes in a list of Herbivore instances (sorted in ascending order of fitness).
        The function lets one carnivore instance hunt (and eat) if conditions in _will_kill are
        satisfied. The function calls eat_fodder  and update_fitness which updates an animal's
        weight and fitness accordingly.

        The remaining Herbivores that are not killed is returned.

        Parameters
        ----------
        herbivore_sorted: list
            List of herbivores sorted in ascending order

        Returns
        -------
        remaining_herbivores: list
            A list of herbivores that are remaining.

        Notes
        -----

        Uses helper function _will_kill which has the probability of kill equal to 0 if

        .. math::
            \\Phi _{carn}<\\Phi _{herb}

        or equal to

        .. math::
            \\frac{\\Phi _{carn}-\\Phi _{herb}}{\\Delta\\Phi _{max}}
        if

        .. math::
            0 < \\Phi _{carn} - \\Phi _{herb} < \\Delta\\Phi _{max}

        or equal 1 otherwise.

        """
        F_carn = self.current_parameters['F']
        remaining_herbivores = []
        amount_eaten = 0
        for herbivore in herbivore_sorted:
            if self._will_kill(herbivore) and amount_eaten < F_carn:
                # Herbivore killed, not included in remaining_herbivores
                amount_eaten += self.eat_fodder(herbivore.weight)
                # Fitness of carnivore updated
                self.update_fitness()
            else:
                # Herbivore not killed, included in remaining herbivores
                remaining_herbivores.append(herbivore)
        return remaining_herbivores
