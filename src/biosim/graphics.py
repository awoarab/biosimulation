"""
:mod:`biosim.graphics` provides graphics support for BioSim.

.. note::
   * This module requires the program ``ffmpeg`` or ``convert``
     available from `<https://ffmpeg.org>` and `<https://imagemagick.org>`.
   * You can also install ``ffmpeg`` using ``conda install ffmpeg``
   * You need to set the  :const:`_FFMPEG_BINARY` and :const:`_CONVERT_BINARY`
     constants below to the command required to invoke the programs
   * You need to set the :const:`_DEFAULT_FILEBASE` constant below to the
     directory and file-name start you want to use for the graphics output
     files.
   *  This module is based on the randvis.graphics module from the INF200 repository
"""
# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2023 Awo Arab & Alin Dak Al-Bab / NMBU

__author__ = 'Awo Arab & Alin Dak Al-Bab'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import numpy as np
import subprocess
import os
from collections import defaultdict

# Update these variables to point to your ffmpeg and convert binaries
# If you installed ffmpeg using conda or installed both softwares in
# standard ways on your computer, no changes should be required.
_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_GRAPHICS_DIR = os.path.join('../../results', 'graphics')
_DEFAULT_GRAPHICS_NAME = 'sample'
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'  # alternatives: mp4, gif


class Graphics:
    """Provides graphics support for BioSim."""

    def __init__(self, img_dir=None, img_name=None, img_fmt=None):
        """
        Initializes a Graphics object. Used by BioSim
        Parameters
        ----------
        img_dir: str
            directory for image files; no images if None
        img_name: str
            beginning of name for image files
        img_fmt: str
            image file format suffix
        """
        # Initialize variables related to plotting
        if img_name is None:
            img_name = _DEFAULT_GRAPHICS_NAME

        if img_dir is not None:
            self._img_base = os.path.join(img_dir, img_name)
        else:
            self._img_base = None

        self._img_fmt = img_fmt if img_fmt is not None else _DEFAULT_IMG_FORMAT
        self._img_ctr = 0
        self._img_step = 1
        # Variables set by setup and _update.
        # Initializes the figure window and Gridspecs-parameters
        self._fig = None
        self.gs = None
        # Initializes variables related to population map
        self._map_ax_h = None
        self._map_ax_c = None
        self._img_axis_h = None
        self._img_axis_c = None
        self.cmax_animals = None
        # Initializes variables related to counter
        self.axt = None
        self.template = None
        self.txt = None
        self.step = None
        # Initializes variables related to total animal count plot
        self._animal_counter_ax = None  # Ax object shared by both animal species
        self._herbs_counter = None  # Empty array (to be filled) for herb.
        self._carns_counter = None  # Empty array (to be filled) for carn
        self.final_step = None
        self.ymax = None
        # Initializes variables related to island map
        self.island_map = None
        self.map_rgb = None
        # Initializes variables related to histograms:
        self.his_specs = None
        self.hist_info_herbs = None
        self.hist_info_carns = None

    def update(self, step, cellwise_herb, cellwise_carn,
               counts_herb, counts_carn, herb_info, carn_info):
        """
        Updates graphics with current data using helper functions. Note that time counter is
        updated in helper function of population maps. Saves image files if necessary.
        Parameters provided by BioSim module.

        Parameters
        ----------
        step: int
            current time in simulation
        cellwise_herb: ndarray
            Numpy array filled with herbivore counts for each cell
        cellwise_carn: ndarray
            Numpy array filled with carnivore counts for each cell
        counts_herb: int
            Total number of herbivores on the island.rst
        counts_carn: int
            Total number of carnivores on the island.rst
        herb_info: dict
            Dictionary with herbivore attributes as keys, and list of values for said attributes
        carn_info: dict
            Dictionary with carnivore attributes as keys, and list of values for said attributes

        .. :note:
            See BioSim.simulate for more details on parameters
        """
        self.step = step
        self._update_system_map(cellwise_herb, cellwise_carn)
        self._update_mean_graph(step=step, count_h=counts_herb, count_c=counts_carn)
        if self.his_specs is not None:
            self._update_histogram(herb_info=herb_info, carn_info=carn_info)
        self._fig.canvas.flush_events()  # ensure every thing is drawn
        plt.pause(1e-6)  # pause required to pass control to GUI
        # Updates final_step so that simulate can rememeber the last simulated year
        self.final_step = step

        self._save_graphics(step)

    def make_movie(self, movie_fmt=None):
        """
        Creates MPEG4 movie from visualization images saved. The movie is stored as
        img_base + movie_fmt

        Parameters
        ----------
        movie_fmt: str
            Movie format. Default is mp4

        Notes
        ------
        - Requires ffmpeg for MP4 and magick for GIF
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt is None:
            movie_fmt = _DEFAULT_MOVIE_FORMAT

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self._img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self._img_base),
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def setup(self, final_step, vis_years, img_step, cmax_animals,
              ymax, island_map, hist_specs=None):
        """
        Prepare graphics. Call this before calling :meth:`update()` for the first time after
        the final time step has changed.

        Parameters
        ----------
        final_step: int
            Last time step to be visualised (upper limit of x-axis)
        vis_years: int
            Interval between visualization updates
        img_step: int
            Interval between saving image to file
        cmax_animals: dict
            Dictionary where keys are animal species and values are cmap-max values passed to
            imshow
        ymax: int
            Limit for y-axis (applied only to total animal counts plot)
        island_map: str
            Multiline string with island.rst georgraphy coded
        hist_specs: dict
            Dictionary specifications for histrogram plots

        Notes
        -----
        - All argument passed from BioSim module.
        For details, see BioSim module documentation
        """
        # Default set in __init__ to 1. Can be changed if img_step
        # is specified
        if img_step is not None:
            self._img_step = img_step

        # Example for GridSpecs inspired by
        # https://matplotlib.org/stable/gallery/subplots_axes_and_figures/gridspec_multicolumn.html#sphx-glr-gallery-subplots-axes-and-figures-gridspec-multicolumn-py
        # Accessed 23.01.23
        if self._fig is None:
            self._fig = plt.figure()  # Create new figure window
            self.gs = GridSpec(9, 9, hspace=0.5)

        # Create setup for island_map
        """Based on code in inf200-course-materials /
        january_block / examples/ plotting / mapping.py"""
        if self.island_map is None:
            island_map_ax = self._fig.add_subplot(self.gs[:3, 3:6])
            self.island_map = island_map
            rgb_value = {'W': (0.0, 0.0, 1.0),  # blue
                         'L': (0.0, 0.6, 0.0),  # dark green
                         'H': (0.5, 1.0, 0.5),  # light green
                         'D': (1.0, 1.0, 0.5)}  # light yellow

            self.map_rgb = [[rgb_value[column] for column in row]
                            for row in island_map.splitlines()]

            island_map_ax.imshow(self.map_rgb)
            island_map_ax.set_title('Island Map')
            island_map_ax.set_xticks(range(0, len(self.map_rgb[0]), 5))
            island_map_ax.set_xticklabels(range(1, 1 + len(self.map_rgb[0]), 5))
            island_map_ax.set_yticks(range(0, len(self.map_rgb), 5))
            island_map_ax.set_yticklabels(range(1, 1 + len(self.map_rgb), 5))

        # Add subplot for population map of herbivores
        if self._map_ax_h is None:
            self._map_ax_h = self._fig.add_subplot(self.gs[:3, :3])  # Subplot for herbivore
            self._img_axis_h = None  # Image axis for herbivore
            self.cmax_animals = cmax_animals
            self._map_ax_h.set_xticks(range(0, len(self.map_rgb[0]), 5))
            self._map_ax_h.set_xticklabels(range(1, 1 + len(self.map_rgb[0]), 5))
            self._map_ax_h.set_yticks(range(0, len(self.map_rgb), 5))
            self._map_ax_h.set_yticklabels(range(1, 1 + len(self.map_rgb), 5))
            self._map_ax_h.set_title('Herbivores Population Map')

        # Add subplot for population map of carnivores
        if self._map_ax_c is None:
            self._map_ax_c = self._fig.add_subplot(self.gs[:3, 6:9])
            self._img_axis_c = None
            self._map_ax_c.set_title('Carnivores Population Map')
            self._map_ax_c.set_xticks(range(0, len(self.map_rgb[0]), 5))
            self._map_ax_c.set_xticklabels(range(1, 1 + len(self.map_rgb[0]), 5))
            self._map_ax_c.set_yticks(range(0, len(self.map_rgb), 5))
            self._map_ax_c.set_yticklabels(range(1, 1 + len(self.map_rgb), 5))
            self._map_ax_c.set_title('Herbivores Population Map')

        # Add subplot for time counter
        if self.axt is None:
            self.axt = self._fig.add_subplot(self.gs[4, 4])
            self.axt.axis('off')  # turn off coordinate system

            self.template = 'Year: {:5d}'
            self.txt = self.axt.text(0.5, 0.5, self.template.format(0),
                                     horizontalalignment='center',
                                     verticalalignment='center',
                                     transform=self.axt.transAxes)

        self.ymax = ymax

        # Add right subplot for line graph of animal counts.
        if self._animal_counter_ax is None:
            self._animal_counter_ax = self._fig.add_subplot(self.gs[4:, :4])
            self._animal_counter_ax.set_ylim(0, self.ymax)
            self._animal_counter_ax.set_title('Total Animal Count')
        # needs updating on subsequent calls to simulate()
        # add 1 so we can show values for time zero and time final_step
        self._animal_counter_ax.set_xlim(0, final_step + 1)

        # Fill subplot with empty nan values for herbivore counts
        if self._herbs_counter is None:
            if vis_years == 1:
                style = 'b-'
                mean_plot_h = self._animal_counter_ax.plot(np.arange(0, final_step + 1),
                                                           np.full(final_step + 1, np.nan),
                                                           style, label='Herbivore')
                self._herbs_counter = mean_plot_h[0]
            else:
                style = '*'
                mean_plot_h = self._animal_counter_ax.plot(np.arange(0, final_step + 1),
                                                           np.full(final_step + 1, np.nan),
                                                           style, label='Herbivore')
                self._herbs_counter = mean_plot_h[0]
        else:
            x_data, y_data_h = self._herbs_counter.get_data()
            x_new = np.arange(x_data[-1] + 1, final_step + 1)
            if len(x_new) > 0:
                y_new_h = np.full(x_new.shape, np.nan)
                self._herbs_counter.set_data(np.hstack((x_data, x_new)),
                                             np.hstack((y_data_h, y_new_h)))
        # Fill subplot with empty nan values for carnivore counts
        if self._carns_counter is None:
            if vis_years == 1:
                style = 'r-'
                mean_plot_c = self._animal_counter_ax.plot(np.arange(0, final_step + 1),
                                                           np.full(final_step + 1, np.nan),
                                                           style, label='Carnivore')
                self._carns_counter = mean_plot_c[0]
            else:
                style = '*'
                mean_plot_c = self._animal_counter_ax.plot(np.arange(0, final_step + 1),
                                                           np.full(final_step + 1, np.nan),
                                                           style, label='Carnivore')
                self._carns_counter = mean_plot_c[0]

        else:
            x_data, y_data_c = self._carns_counter.get_data()
            x_new = np.arange(x_data[-1] + 1, final_step + 1)
            if len(x_new) > 0:
                y_new_c = np.full(x_new.shape, np.nan)
                self._carns_counter.set_data(np.hstack((x_data, x_new)),
                                             np.hstack((y_data_c, y_new_c)))

        # Create setup for herbivore histograms
        if hist_specs is not None:
            # Collecting ax and bin info for each animal attribute specified in hist_specs
            self.his_specs = hist_specs
            self.hist_info_herbs = defaultdict(list)
            self.hist_info_carns = defaultdict(list)
            for i, key in enumerate(list(hist_specs.keys())):
                hist_ax = self._fig.add_subplot(self.gs[4 + (2 * i - 1):6 + (2 * i - 1), 5:])
                hist_ax.clear()
                bin_max = hist_specs[key]['max']
                bin_width = hist_specs[key]['delta']
                hist_ymax = 4000
                bin_edges = np.arange(0, bin_max + bin_width / 2, bin_width)
                hist_counts = np.zeros_like(bin_edges[:-1], dtype=float)
                hist_herb = hist_ax.stairs(hist_counts, bin_edges, color='b', lw=2)
                hist_carn = hist_ax.stairs(hist_counts, bin_edges, color='r', lw=2)
                # Appending to dictionary to be used in _update_histogram
                self.hist_info_herbs[key].append(hist_herb)
                self.hist_info_herbs[key].append(bin_edges)
                self.hist_info_carns[key].append(hist_carn)
                self.hist_info_carns[key].append(bin_edges)

                hist_ax.set_ylim([0, hist_ymax], auto=True)
                hist_ax.set_title(f'{key}')

    def _update_system_map(self, sys_map_herb, sys_map_carn):
        """
        Helper function for Graphics.update method
        Updates populations maps and time counter. Makes unique maps for each species

        Parameters
        ----------
        sys_map_herb: ndarray
            Numpy array (see Graphics.setup method)
        sys_map_carn: ndarray
            Numpy array (see Graphics.setup method)
        """
        # Herbivores population map
        if self._img_axis_h is not None:
            self._img_axis_h.set_data(sys_map_herb)
        else:
            self._img_axis_h = self._map_ax_h.imshow(sys_map_herb,
                                                     interpolation='nearest',
                                                     vmin=-0,
                                                     vmax=self.cmax_animals['Herbivore'])
            plt.colorbar(self._img_axis_h, ax=self._map_ax_h,
                         orientation='vertical')

        # Carnivores population map
        if self._img_axis_c is not None:
            self._img_axis_c.set_data(sys_map_carn)
        else:
            self._img_axis_c = self._map_ax_c.imshow(sys_map_carn,
                                                     interpolation='nearest',
                                                     vmin=-0,
                                                     vmax=self.cmax_animals['Carnivore'])
            plt.colorbar(self._img_axis_c, ax=self._map_ax_c,
                         orientation='vertical')
        # Plots time counter
        if self.axt is not None:
            k = self.step + 1
            self.txt.set_text(self.template.format(k))
            plt.pause(0.1)  # pause required to make update visible

    def _update_mean_graph(self, step, count_h, count_c):
        """
        Helper function for Graphics.update method
        Updates graph for total animal counts.

        Parameters
        ----------
        step: int
            Current year
        count_h: int
            Herbivore count
        count_c: int
            Carnivore count
        """
        if count_h + 10 < self.ymax and count_c + 10 < self.ymax:
            y_data_h = self._herbs_counter.get_ydata()
            y_data_h[step] = count_h
            self._herbs_counter.set_ydata(y_data_h)

            y_data_c = self._carns_counter.get_ydata()
            y_data_c[step] = count_c
            self._carns_counter.set_ydata(y_data_c)
        # Create new graphics window and increase y-axis
        else:
            self.ymax = max(count_h, count_c) + 100
            self._animal_counter_ax.set_ylim(0, self.ymax)
            y_data_h = self._herbs_counter.get_ydata()
            y_data_h[step] = count_h
            self._herbs_counter.set_ydata(y_data_h)

            y_data_c = self._carns_counter.get_ydata()
            y_data_c[step] = count_c
            self._carns_counter.set_ydata(y_data_c)
        self._animal_counter_ax.legend()

    def _update_histogram(self, herb_info, carn_info):
        """
        Helper function for Graphics.update method.
        Updates histograms. Creates unique histogram for each animal attribute

        Parameters
        ----------
        herb_info: dict
            Dictionary with herbivore attributes
        carn_info: dict
            Dictionary with carnivore attributes
        """
        for key in list(self.hist_info_herbs):
            hist_count_herb, _ = np.histogram(herb_info[key],
                                              self.hist_info_herbs[key][1])
            self.hist_info_herbs[key][0].set_data(hist_count_herb)
        for key in list(self.hist_info_carns):
            hist_count_carn, _ = np.histogram(carn_info[key], self.hist_info_carns[key][1])
            self.hist_info_carns[key][0].set_data(hist_count_carn)

    def _save_graphics(self, step):
        """
        Helper function for Graphics.update method.
        Save figure generated by Graphics.

        Parameters
        ----------
        step: int
            Interval for which figure should be saved.
        """

        if self._img_base is None or step % self._img_step != 0:
            return

        plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                     num=self._img_ctr,
                                                     type=self._img_fmt))
        self._img_ctr += 1
