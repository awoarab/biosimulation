# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2023 Awo Arab & Alin Dak Al / NMBU

__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

from .landscape import Water, Lowland, Highland, Desert
import random
from collections import defaultdict
import numpy as np


class Island:
    """Island class"""
    default_parameters = {"W": Water,
                          "L": Lowland,
                          "H": Highland,
                          "D": Desert}

    def __init__(self, map_string):
        """
        Checks if parameters in map_string are like default_parameters using helper function
        _correct_params. If so, it creates a full map (with water coordinates), and a list of
        valid dry land coordinates. Also creates an empty list of migrants. If parameters not
        alike, then raises a ValueError

        Parameters
        ----------
        map_string: str
            Multiline string specifying the island's geography. Allowed symbols are W,D,L,H

        Notes
        -----
        Following is an example how an island is initialized:

        .. code::

            island = Island(map_string = 'WWW\\nWLW\\nWWW')

        """
        self.row = 1
        self.col = 1
        self._correct_params(map_string)  # Checks if map_string has valid symbols
        self._correct_dims(map_string)  # Checks if the map_string has correct dimensions
        self._bad_boundary_col(map_string)  # Checks bad boundaries (cols)
        self._bad_boundary_row(map_string)  # Checks bad boundaries (rows)
        self.full_map = self._create_full_map(map_string)
        self.dryland_coors = self._filter_water()
        self.seasonal_migrants = defaultdict(list)
        self.herbivore_info = defaultdict(list)
        self.carnivore_info = defaultdict(list)

    def _correct_params(self, map_string):
        """
        Check whether the letters in map_string inputted by the user contains has the same
        letters as in default_parameters.

        Parameters
        ----------
        map_string: str
            Multiline string specifying the island's geography. Allowed symbols are W,D,L,H

        Raises
        -------
        ValueError
            If there are any unknown symbols in map_string.
        """
        valid_symbols = list(self.default_parameters.keys())
        valid_symbols.append("\n")
        for letter in list(map_string):
            if letter not in valid_symbols:
                raise ValueError('Invalid parameter. Valid parameter letters are',
                                 self.default_parameters)

    @staticmethod
    def _correct_dims(map_string):
        """
        A helper function to check if all rows in map_string has the same length. Raise a
        ValueError if it is not the case.

        Parameters
        ----------
        map_string: str
            Multiline string specifying the island's geography. Allowed symbols are W,D,L,H

        Raises
        -------
        ValueError
            If all rows have different lengths
        """
        island_strs = map_string.split("\n")
        len_check = len(island_strs[0])
        for element in island_strs:
            if len(element) != len_check:
                raise ValueError('All rows must have equal length')

    @staticmethod
    def _bad_boundary_col(map_string):
        """
        A helper function for boundaries. It checks that only water is in the first and last column.
        Raises a ValueError if that is not the case.

        Parameters
        ----------
        map_string: str
            Multiline string specifying the island's geography. Allowed symbols are W,D,L,H

        Raises
        -------
        ValueError
            If dryland is in the first and last column instead of water.
        """
        island_str = map_string.split("\n")
        for element in island_str:
            if element[0] != 'W' or element[-1] != 'W':
                raise ValueError('Bad boundary. Island boundaries has to be W')

    @staticmethod
    def _bad_boundary_row(map_string):
        """
        A helper function for boundaries. It checks that only water is in the first and last row.
        Raises a ValueError if that is not the case.

        Parameters
        ----------
        map_string: str
            Multiline string specifying the island's geography. Allowed symbols are W,D,L,H

        Raises
        -------
        ValueError
            If dryland is in the first and last row instead of water.
        """
        island_str = map_string.split("\n")
        top_row = island_str[0]
        bottom_row = island_str[-1]
        for letter in top_row:
            if letter != 'W':
                raise ValueError('Bad boundary. Island boundaries has to be W')
        for letter in bottom_row:
            if letter != 'W':
                raise ValueError('Bad boundary. Island boundaries has to be W')

    def _create_full_map(self, map_string):
        """
        Function identifying how to read the map_string by increasing the row or columns number,
        then based on that creating the full_map.

        Parameters
        ----------
        map_string: str
            Multiline string specifying the island's geography. Allowed symbols are W,D,L,H

        Returns
        -------
        full_map: dict
            A dictionary containing location coordinates
        """
        full_map = {}
        for element in map_string:
            landscape_cell = self.default_parameters.get(element, 0)
            if landscape_cell == 0:
                self.row += 1
                self.col = 1
            else:
                location = (self.row, self.col)
                full_map[location] = landscape_cell(self.row, self.col)
                self.col += 1
        return full_map

    def _filter_water(self):
        """
        A helper function that filters out water, so only considers dryland (Highland, loweland,
        and desert)

        Returns
        -------
        dry_land_map.keys(): list
            A list of keys for the dry_land_map
        """
        # Inspired by:
        # https://thispointer.com/python-filter-a-dictionary-by-conditions-on-keys-or-values/
        # Accessed 17.01.2023 [18:02]
        dry_land_map = dict(filter(lambda item: type(item[1]) is not Water, self.full_map.items()))
        return list(dry_land_map.keys())

    def populate_cell(self, ini_animals):
        """
        Checks if ini_animals contain the valid_keys. If not, raise a keyError. Also places the
        animals in the correct location. Note that location cannot be None, hence if
        location is None, we raise a valueError

        Parameters
        ----------
        ini_animals: List of dictionaries
            Dictionary containing loc and pop

        Raises
        -------
        KeyError
            If population_list.keys() is not in valid_keys
        ValueError
            If location is water (not dryland)

        Notes
        -----
        Following is an example of how ini_animals list should look:

        .. code::

            [{'loc': (10, 10), 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20},
            {'loc': (10, 10), 'pop': [{'species': 'Carnivore', 'age': 2, 'weight': 15}]

        """
        valid_keys = ['loc', 'pop']
        for population_list in ini_animals:
            keys_present = list(population_list.keys())
            # First check that both pop and loc are present
            if not all([key in valid_keys for key in keys_present]):
                raise KeyError('Invalid parameters given. Only parameters allowed', valid_keys)
            # Otherwise begin extracting coordinates and list of animals
            else:
                location = population_list['loc']
                population = population_list['pop']
                # Check if the location specified is a dryland coordinate
                # Animals not added to cells that are not present
                if location not in self.dryland_coors:
                    raise ValueError(f' No habitable at {location}. No population added there')
                else:
                    self.full_map[location].add_animals(population)

    def count_herbivores(self):
        """
        Function that counts the total number of herbivores in each cell as well as the total
        number of herbivores.

        Returns
        -------
        herb_total: int
            Total number of herbivores.
        herb_cellwise: ndarray
            Number of herbivore per cell.
        """
        herb_cellwise = np.zeros((self.row, self.col))
        herb_total = 0
        for coor in self.dryland_coors:
            herb_cellwise[coor] = self.full_map[coor].count_herbs_in_cell()
            herb_total += self.full_map[coor].count_herbs_in_cell()
        return herb_total, herb_cellwise

    def count_carnivores(self):
        """
        Function that counts the total number of carnivores in each cell as well as the total
        number of carnivores.

        Returns
        -------
        carn_total: int
            Total number of carnivores.
        carn_cellwise: ndarray
             Number of carnivores per cell.
        """
        carn_cellwise = np.zeros((self.row, self.col))
        carn_total = 0
        for coor in self.dryland_coors:
            carn_cellwise[coor] = self.full_map[coor].count_carns_in_cell()
            carn_total += self.full_map[coor].count_carns_in_cell()
        return carn_total, carn_cellwise

    def create_animal_info_dict(self):
        for coor in self.dryland_coors:
            herbivore_info = self.full_map[coor].extract_herbivore_info()
            carnivore_info = self.full_map[coor].extract_carnivore_info()
            for herbivore in herbivore_info:
                self.herbivore_info['age'].append(herbivore['age'])
                self.herbivore_info['weight'].append(herbivore['weight'])
                self.herbivore_info['fitness'].append(herbivore['fitness'])
            for carnivore in carnivore_info:
                self.carnivore_info['age'].append(carnivore['age'])
                self.carnivore_info['weight'].append(carnivore['weight'])
                self.carnivore_info['fitness'].append(carnivore['fitness'])

        return self.herbivore_info, self.carnivore_info

    def seasonal_update_island(self):
        """
        Function that annually updates the island's cells.
        The cycle is in accordance with the  description in section 2.3.
        """
        # For each cell, manage the animal reproduction and feeding
        for coor in self.dryland_coors:
            self.full_map[coor].allow_reproduction()
            self.full_map[coor].feed_herbivores()
            self.full_map[coor].feed_carnivores()
        # Collect migrants in self.seasonal_migrants
        self.allow_migration()
        # Distribute the seasonal migrants to their destination cell
        self.distribute_seasonal_migrants()
        # For each cell, manage fodder regrowth, animal weight loss/aging and death of animals
        for coor in self.dryland_coors:
            self.full_map[coor].seasonal_update_landscape()
        # Set dictionnaries used to track animals to empty
        self.seasonal_migrants = defaultdict(list)
        self.herbivore_info = defaultdict(list)
        self.carnivore_info = defaultdict(list)

    @staticmethod
    def _assign_direction(loc, pot_migrants):
        """
        A helper function that assigns direction for potential migration and randomly chooses
        the direction the animal moves. Also created a new list with the new direction.

        Parameters
        ----------
        loc: int
            Coordinate for cell
        pot_migrants: list
            List of potential migrating animals

        Returns
        -------
        new_coors: list
            New cell coordinates of animal location.
        """
        # noinspection PyTypeChecker
        n_dir = len(pot_migrants)
        directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        new_coors = []
        for i in range(n_dir):
            chosen_dir = random.choice(directions)
            # noinspection PyUnresolvedReferences
            new_coors.append((loc[0] + chosen_dir[0], loc[1] + chosen_dir[1]))
        return new_coors

    @staticmethod
    def _check_any_migrants(migrant_list):
        """
        A helper function that check whether the migrant_list is empty.

        Parameters
        ----------
        migrant_list: list
            List of potential migrating animals

        Returns
        -------
        True or False: boolean type
        """
        # noinspection PyTypeChecker
        if len(migrant_list) > 0:

            return True
        else:
            return False

    def allow_migration(self):
        """
        Function that looks at whether the chosen cell is valid for migration and assigns direction
        randomly accordingly Function then updates the cell automatically.
        """
        land_coordinates = self.dryland_coors
        # Inspired by:
        # https://www.geeksforgeeks.org/python-initializing-dictionary-with-empty-lists/
        # Accessed 17.01.2023

        # We loop only through dry_land coordinates
        for coor in land_coordinates:
            # For each Landscape object, we call create migrant to get list of potential migrants
            potential_migs = self.full_map[coor].create_migrant_list()
            non_migrants = []  # Used to collect animals that have been assigned invalid new_coor
            # We check if there are any new potential migrants
            if self._check_any_migrants(potential_migs):
                # If there are, we create a list of random N,S,E,W directions
                assigned_dir = self._assign_direction(coor, potential_migs)
                # We loop through new cooridnates and potential migrants
                for new_coor, animal in zip(assigned_dir, potential_migs):
                    if new_coor in land_coordinates:
                        self.seasonal_migrants[new_coor].append(animal)
                    else:
                        # Collect a list of animals that have chosen invalid new_coor
                        non_migrants.append(animal)
                # Re-add animals that have chosen invalid new_coor
                self.full_map[coor].add_migrants(non_migrants)

    def distribute_seasonal_migrants(self):
        """
        A function for placing the migrating animals into their new cells.
        """
        for loc, animal_list in self.seasonal_migrants.items():
            self.full_map[loc].add_migrants(animal_list)
