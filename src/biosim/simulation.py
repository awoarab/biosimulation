# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2023 Hans Ekkehard Plesser / NMBU
__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

"""
Implements and controls simulations for biosim-package
"""

from .animals import Herbivore, Carnivore
from .landscape import Desert, Lowland, Highland, Water
from .island import Island
from .graphics import Graphics
import random
import csv
from os import path


class BioSim:
    """
    Top-level interface to BioSim package.
    """

    def __init__(self, island_map, ini_pop, seed,
                 vis_years=1, ymax_animals=None, cmax_animals=None, hist_specs=None,
                 img_years=None, img_dir=None, img_base=None, img_fmt='png',
                 log_file=None):

        """
        Parameters
        ----------
        island_map : str
            Multi-line string specifying island geography
        ini_pop : list
            List of dictionaries specifying initial population
        seed : int
            Integer used as random number seed
        vis_years : int
            Years between visualization updates (if 0, disable graphics)
        ymax_animals : int
            Number specifying y-axis limit for graph showing animal numbers
        cmax_animals : dict
            Color-scale limits for animal densities, see below
        hist_specs : dict
            Specifications for histograms, see below
        img_years : int
            Years between visualizations saved to files (default: `vis_years`)
        img_dir : str
            Path to directory for figures
        img_base : str
            Beginning of file name for figures
        img_fmt : str
            File type for figures, e.g. 'png' or 'pdf'
        log_file : str
            If given, write animal counts to this file

        Notes
        -----
        - If `ymax_animals` is None, the y-axis limit should be adjusted automatically.
        - If `cmax_animals` is None, sensible, fixed default values should be used.
        - `cmax_animals` is a dict mapping species names to numbers, e.g.,

          .. code:: python

             {'Herbivore': 50, 'Carnivore': 20}

        - `hist_specs` is a dictionary with one entry per property for which a histogram
          shall be shown. For each property, a dictionary providing the maximum value
          and the bin width must be given, e.g.,

          .. code:: python

             {'weight': {'max': 80, 'delta': 2},
              'fitness': {'max': 1.0, 'delta': 0.05}}

          Permitted properties are 'weight', 'age', 'fitness'.
        - If `img_dir` is None, no figures are written to file.
        - Filenames are formed as

          .. code:: python

             Path(img_dir) / f'{img_base}_{img_number:05d}.{img_fmt}'

          where `img_number` are consecutive image numbers starting from 0.

        - `img_dir` and `img_base` must either be both None or both strings.

        Following is an example of how BioSim can be initialized:

        .. code::

           sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                seed=123456, hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                'age': {'max': 60.0, 'delta': 2},
                'weight': {'max': 60, 'delta': 2}}, vis_years=1)

        """
        self.island_map = island_map
        self.island = Island(map_string=island_map)
        self.island.populate_cell(ini_pop)  # Cells populated with the initial population
        self.seed = seed
        self.vis_years = vis_years
        self.hist_specs = hist_specs
        self.img_dir = img_dir
        self.img_base = img_base
        self.img_fmt = img_fmt
        self.log_file = log_file
        self._year = 0  # Initialize last year simulated with 0 years
        self._graphics = Graphics(img_dir, img_base, img_fmt)  # Initialize a Graphics objet
        if img_years is None:
            self.image_year = vis_years
        else:
            self.image_year = img_years
        if ymax_animals is None:
            self.ymax_animals = 4000
        else:
            self.ymax_animals = ymax_animals
        if cmax_animals is None:
            self.cmax_animals = {'Herbivore': 200, 'Carnivore': 50}
        else:
            self.cmax_animals = cmax_animals

    def set_animal_parameters(self, species, params):
        """
        Set parameters for animal species.

        Parameters
        ----------
        species : str
            Name of species for which parameters shall be set.
        params : dict
            New parameter values

        Raises
        ------
        ValueError
            If invalid parameter values are passed.

        Notes
        -----
        Following is example of how one can set animal parameters for a BioSim instance:

        .. code::

            sim.set_animal_parameters('Herbivore', {'zeta': 3.2, 'xi': 1.8})
            sim.set_animal_parameters('Carnivore', {'a_half': 70, 'phi_age': 0.5,
                                            'omega': 0.3, 'F': 65, 'DeltaPhiMax': 9.})

        """
        valid_species = {'Herbivore': Herbivore, 'Carnivore': Carnivore}
        if species in list(valid_species.keys()):
            valid_species[species].set_params(params)
        else:
            raise ValueError('Invalid species. Only Herbivore and Carnivore are valid species')

    def set_landscape_parameters(self, landscape, params):
        """
        Set parameters for landscape type.

        Parameters
        ----------
        landscape : str
            Code letter for landscape
        params : dict
            New parameter values

        Raises
        ------
        ValueError
            If invalid parameter values are passed.

        Notes
        -----
        Following is an example of how one can set landscape parameters for a BioSim instance:

        .. code::

            sim.set_landscape_parameters('L', {'f_max': 700})

        """
        valid_letters = {'L': Lowland, 'H': Highland,
                         'W': Water, 'D': Desert}
        if landscape in list(valid_letters.keys()):
            valid_letters[landscape].set_params(params)
        else:
            raise ValueError(f'Invalid letter-code. Only \
            {list(valid_letters.keys())} are valid letters.')

    def simulate(self, num_years):
        """
        Run simulation while visualizing the result if graphics is not None. Log-file create
        if logfile is not None.

        Parameters
        ----------
        num_years : int
            Number of years to simulate

        Notes
        -----

        If one has specified hist_specs, a window with following plots should be generated:

        .. image:: simulation_output.png

        """
        start_year = self._year
        end_year = self._year + num_years
        random.seed(self.seed)
        if self.vis_years != 0:
            self._graphics.setup(final_step=end_year, vis_years=self.vis_years,
                                 img_step=self.image_year, cmax_animals=self.cmax_animals, ymax=100,
                                 island_map=self.island_map, hist_specs=self.hist_specs)
        for year in range(start_year, end_year):
            # Take count at beginning of the year
            herb_total, herb_cellwise = self.island.count_herbivores()
            carn_total, carn_cellwise = self.island.count_carnivores()
            herb_info, carn_info = self.island.create_animal_info_dict()

            # Updating graphics is vis_year is not 0
            if self.vis_years != 0:
                if year in range(start_year, end_year, self.vis_years):
                    self._graphics.update(step=year, cellwise_herb=herb_cellwise,
                                          cellwise_carn=carn_cellwise, counts_herb=herb_total,
                                          counts_carn=carn_total, herb_info=herb_info,
                                          carn_info=carn_info)
            # Writing to file if log_file is not None:
            if self.log_file is not None:
                self._log_and_save_animal_counts(self.log_file, self.seed,
                                                 year, herb_total, carn_total)
            # Going through the island's seasons
            self.island.seasonal_update_island()

        # Update number of simulated years
        self._year += num_years

    @staticmethod
    def _log_and_save_animal_counts(log_file, seed, year, h_count, c_count):
        """
        Helper method used in simulate-method. All parameter provied by simulate method.
        Creates a csv-file.

        Parameters
        ----------
        log_file: str
            Logfile to write to.
        seed: int
            Seed used in random number generator
        year: int
            Year to be filled in year column
        h_count: int
            Number of herbivores to be filled in Herbivore Count column
        c_count: int
            Number of carnivores to be filled in Carnivore Count column
        """
        # If log-file (with same seed) does not exist, we will write to it:
        if not path.exists(f'{log_file}_{seed}.csv'):
            # Code that is  writing to csv-file inspired by:
            # https://www.scaler.com/topics/how-to-create-a-csv-file-in-python/  13.01.2023
            with open(f'{log_file}_{seed}.csv', 'w') as file:
                writer = csv.writer(file)
                writer.writerow(['Year', 'Herbivore Count', 'Carnivore Count'])
                writer.writerow([f'{year}', f'{h_count}', f'{c_count}'])
        # If a log-file of the same seed exists, we will not create new one (append to it)
        else:
            with open(f'{log_file}_{seed}.csv', 'a') as file:
                writer = csv.writer(file)
                writer.writerow([f'{year}', f'{h_count}', f'{c_count}'])

    def add_population(self, population):
        """
        Add a population to the island

        Parameters
        ----------
        population : List of dictionaries

        Notes
        -----
        The function takes in a dictionary in the following format

        .. code::

            ini_herbs = [{'loc': (10, 10), 'pop': [{'species': 'Herbivore','age': 5,
                                'weight': 20} for _ in range(150)]}]

        """
        self.island.populate_cell(population)

    @property
    def year(self):
        """Last year simulated."""
        return self._year

    @property
    def num_animals(self):
        """Total number of animals on island."""
        return self.island.count_herbivores()[0] + self.island.count_carnivores()[0]

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary"""
        return {'Herbivore': self.island.count_herbivores()[0],
                'Carnivore': self.island.count_carnivores()[0]}

    def make_movie(self):
        """Create MPEG4 movie from visualization images saved."""
        self._graphics.make_movie()
