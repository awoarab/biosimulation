# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2023 Awo Arab & Alin Dak Al / NMBU

__author__ = 'Awo Arab & Alin Dak'
__email__ = 'awo.mohamed.osman.arab@nmbu, alin.dak.al-bab@nmbu.no'

import random
import itertools
from numpy import logical_not

from .animals import Herbivore
from .animals import Carnivore


class Landscape:
    """Landscape superclass"""

    @classmethod
    # Class method based on examples in lecture notes (bacteria.py)
    def set_params(cls, new_params):
        """
        Can be used to set landscape parameters. See table 3 for default parameters

        Parameters
        ----------
        new_params: dict
            Dictionary, parameters for animal type.

        Raises
        -------
        KeyError:
            If key is not defined in default_params.
        ValueError:
            If key does not meet criteria (see table 2 in project description).

        Notes
        ------
        Landscape parameter can be set using BioSim interface. If one wants to set a
        single landscape, then use this method in the following way:

        .. code:: python

             Landscape.set_params({'f_max': 0})

        """
        for key in new_params:
            # noinspection PyUnresolvedReferences
            if key not in list(cls.current_parameters.keys()):
                raise KeyError('Invalid parameter name: ' + key)
            elif new_params[key] < 0:
                raise ValueError('Parameter must be equal to or greater than 0 ' + key)
            else:
                # noinspection PyUnresolvedReferences
                cls.current_parameters[key] = new_params[key]

    @classmethod
    # Class method based on examples in lecture notes (bacteria.py)
    def get_params(cls, param_key=None):
        """
        Gets the parameters entered by the user.
        If param_key is None, then all current parameters are returned
        Parameters
        ----------
        param_key: str
            A key of the dictionary

        Returns
        -------
        current_parameters: dict
            Dictionary with current parameters for Herbivore class.

        Notes
        -----
        The BioSim interface should be used to get parameter for the whole island.
        If one wants to retrieve parameters for an entire

        .. code:: python

             Landscape.get_params('f_max')
        """
        if param_key is None:
            # noinspection PyUnresolvedReferences
            return cls.current_parameters
        else:
            # noinspection PyUnresolvedReferences
            return cls.current_parameters[param_key]

    def __init__(self, x, y):
        """
        Landscape instance created with location, fodder amount and animals lists as attributes.
        Note that row and column numbers have to start with 1.

        Parameters
        ----------
        x: int
            row number
        y: int
            column number
        """
        self.x = x
        self.y = y
        self.herbivores_list = []
        self.carnivores_list = []
        # noinspection PyUnresolvedReferences
        self.fodder_available = self.default_parameters['f_max']

    @staticmethod
    def _identify_and_create_animal(species_name, age, weight):
        """
        Helper which identifies the subclass of Animal and creates instance.
        Fitness is set (only done for the first population added).

        Parameters
        ----------
        species_name: str
            Has to be Herbivore or Carnivore
        age: int
            See Animals module for specifications
        weight: int
            See Animals module for specifications

        Returns
        -------
        herbivore: Herbivore
            One Herbivore instance with age and weight given
        carnivore: Carnivore
            One Carnivore instance with age and weight given
        """
        if species_name == 'Herbivore':
            herbivore = Herbivore(age, weight)
            herbivore.update_fitness()
            return herbivore
        # To be defined later
        elif species_name == 'Carnivore':
            carnivore = Carnivore(age, weight)
            carnivore.update_fitness()
            return carnivore

    def add_animals(self, population):
        """
        Function that checks if animal information in population has the valid keys and the
        correct species type. Function then takes in 'pop'-argument in ini_pop_lists, and adds
        animals to the landscape cell. Helper _identify_and_create_animal checks the subclass
        of the animal and adds it to the correct list.

        Parameters
        ----------
        population: List of dictionaries
            Dictionary containing the species type, age, and weight

        Raises
        -------
        KeyError
            If parameters other than species, age and weight are given
        ValueError
            If species type is not Herbivore or Carnivore.

        Notes
        -----
        The BioSim interface takes care of populating the individual cells with animals.
        If one wants to add animal's to a specific location, see biosim module
        """
        valid_keys = ['species', 'age', 'weight']
        valid_species = ['Herbivore', 'Carnivore']
        for animal_info in population:
            if not all(key in valid_keys for key in list(animal_info.keys())):
                raise KeyError('Invalid parameters given. Only parameters allowed', valid_keys)
            elif animal_info['species'] not in valid_species:
                raise ValueError('Invalid species. Only Herbivore and Carnivore allowed')

            else:
                animal = self._identify_and_create_animal(animal_info['species'],
                                                          animal_info['age'],
                                                          animal_info['weight'])
                if animal_info['species'] == 'Herbivore':
                    self.herbivores_list.append(animal)
                else:
                    self.carnivores_list.append(animal)

    def _check_herbivores_present(self):
        """
        A helper function that check if herbivore list not empty.

        Returns
        -------
        True or False: boolean type
        """
        if len(self.herbivores_list) > 0:
            return True
        else:
            return False

    def _check_carnivore_present(self):
        """
        A helper function that check if carnivore list not empty.

        Returns
        -------
        True or False: boolean type
        """
        if len(self.carnivores_list) > 0:
            return True
        else:
            return False

    def feed_herbivores(self):
        """
        Function that randomly shuffles the herbivores_instance_list and allows them to feed
        in that order. Should reduce amount of fodder available. Uses helpers
        _check_herbivores_present and feeds animals in cells if it returns True.

        """
        # noinspection PyUnresolvedReferences
        self.fodder_available = self.current_parameters['f_max']
        if self._check_herbivores_present():
            random.shuffle(self.herbivores_list)
            for herbivore in self.herbivores_list:
                if self.fodder_available > 0:
                    amount_eaten = herbivore.eat_fodder(self.fodder_available)
                    self.fodder_available -= amount_eaten
                else:
                    break

    def feed_carnivores(self):
        """
        Function that feeds all the carnivores in the same landscape cell if both animal
        types are present (uses _check_herbivores_present and _check_carnivore_present).

        Animal fitness is updated first.The function sorts carnivores is descending order of
        fitness and herbivores in ascending order of fitness.

        It then calls hunt_and_feed function in animal.py to feed one carnivore at a time.
        The list of sorted herbivore instances are given to hunt_and_feed, and list of available
        herbivores are updated accordingly.
        """
        if self._check_herbivores_present() and self._check_carnivore_present():
            # Update the fitness of all animals:
            [carnivore.update_fitness() for carnivore in self.carnivores_list]
            [herbivore.update_fitness() for herbivore in self.herbivores_list]
            self.carnivores_list.sort(key=lambda item: item.fitness, reverse=True)
            self.herbivores_list.sort(key=lambda item: item.fitness)
            for carnivore in self.carnivores_list:
                remaining_herbivores = carnivore.hunt_and_feed(self.herbivores_list)
                self.herbivores_list = remaining_herbivores

    @staticmethod
    def _one_species_reproduction(animal_list):
        """
        A helper function used to create a list of newborns depending on the type of animals
        contained in animal_list.

        Parameters
        ----------
        animal_list: list
            List of Animal class instances.

        Returns
        -------
        animal_list: list
            An updated animal_list with newborns appended to the Landscape cell's animal lists
        """
        # noinspection PyTypeChecker
        n_animals = len(animal_list)
        newborns = [animal.reproduce(n_animals) for animal in animal_list]
        # Inspired by https://www.geeksforgeeks.org/python-remove-none-values-from-list/
        # Accessed 12.01.2023 13:17
        # Filters out None (output from herbivore that did not give birth)
        newborns = list(filter(lambda item: item is not None, newborns))
        # Appends newborn-instances to list of herbivore instances
        # noinspection PyUnresolvedReferences
        return [animal_list.append(newborn) for newborn in newborns]

    def allow_reproduction(self):
        """
        Function that calls the reproduce method in Animals-class. Uses helper function
        _one_species_reproduction. Function is ran only if there are at least two animals
        of the same species in the same cell.
        """
        for animal_list in [self.herbivores_list, self.carnivores_list]:
            if len(animal_list) > 1:
                self._one_species_reproduction(animal_list)

    @staticmethod
    def _create_migrants(animal_list):
        """
        A helper function that creates two list, one for animal who will potentially migrate and
        another for the animals who will remain where they are (non-migrant animals). The method
        call the will_migrate method in Animal module.

        Parameters
        ----------
        animal_list: list
            List of Animal class objects.

        Returns
        -------
        potential-migrants: list
            Animal who might migrate.
        non_migrants: list
            Animals who will not migrate.
        """
        migration_status = [animal.will_migrate() for animal in animal_list]
        potential_migrants = list(itertools.compress(animal_list, migration_status))
        non_migrants = list(itertools.compress(animal_list, logical_not(migration_status)))
        return potential_migrants, non_migrants

    def create_migrant_list(self):
        """
        Function that calls the helper function _help_create_migrants to identify the migrating
        herbivores and carnivores and creates a new variable for all migrating animals.

        Returns
        -------
        potential_migrants: list,
            All potentially migrating animals
        []: list
            An empty list
        """
        if self._check_herbivores_present() or self._check_carnivore_present():
            potential_migrants_h, self.herbivores_list = self._create_migrants(self.herbivores_list)
            potential_migrants_c, self.carnivores_list = self._create_migrants(self.carnivores_list)
            potential_migrants = potential_migrants_c + potential_migrants_h
            return potential_migrants
        else:
            return []

    def add_migrants(self, animal_list):
        """
        Appends migrants to the appropriate list of animals (see Landscape attributes).
        The method is used in the Island module to distribute migrants.This method differs
        from the add_animal method (which takes in a list of dictionaries)

        Parameters
        ----------
        animal_list: list
            List of Animal class instances.

        Notes
        -----
        Following is an example of how Island module uses this method:

        .. code::

            migrants = [Carnivore(10,10) for _ in range(50)]
            Landscape(x = 2,y = 3 ).add_migrants(migrants)

        """
        for animal in animal_list:
            if type(animal) is Herbivore:
                self.herbivores_list.append(animal)
            else:
                self.carnivores_list.append(animal)

    def count_herbs_in_cell(self):
        """
        Function that counts the number of herbivore in the cell.
        Can be used instead of BioSim interface to extract no. of herbivores in 1 specific cell

        Returns
        -------
        herb_count: int
            No. of herbivores in herbivore_list
        """
        herb_count = len(self.herbivores_list)
        return herb_count

    def count_carns_in_cell(self):
        """
        Function that counts the number of carnivore in the cell.
        Can be used instead of BioSim interface to extract no. of carnivores  in 1 specific cell

        Returns
        -------
        carn_count: int
            No of carnivores in carnivore_list
        """
        carn_count = len(self.carnivores_list)
        return carn_count

    def extract_herbivore_info(self):
        """
        Function that extracts Herbivore attributes in Landscape.herbivores_list
        Called  by Island instance

        Returns
        -------
        herb_info: list
            List of dictionaries that each contain Herbivore attributes
        """
        herb_info = [herbivore.__dict__ for herbivore in self.herbivores_list]
        return herb_info

    def extract_carnivore_info(self):
        """
        Function that extracts Carnivore attributes in Landscape.carnivores_list
        Called by Island instance

        Returns
        -------
        carn_info: list
            List of dictionaries that each contain Carnivore attributes
        """
        carn_info = [carnivore.__dict__ for carnivore in self.carnivores_list]
        return carn_info

    def seasonal_update_landscape(self):
        """
        Function that:
        1. Regrows fodder at the end of the year
        2. Run seasonal updates for animals in the cell
        3. Removes animals that have died
        """
        # noinspection PyUnresolvedReferences
        self.fodder_available = self.current_parameters['f_max']
        if len(self.herbivores_list) > 0:
            is_alive_herbs = [herbivore.seasonal_update_animal()
                              for herbivore in self.herbivores_list]
            self.herbivores_list = list(itertools.compress(self.herbivores_list, is_alive_herbs))
        if len(self.carnivores_list) > 0:
            is_alive_carns = [carnivore.seasonal_update_animal()
                              for carnivore in self.carnivores_list]
            self.carnivores_list = list(itertools.compress(self.carnivores_list, is_alive_carns))


class Highland(Landscape):
    """Highland (subclass)"""
    default_parameters = {'f_max': 300}
    current_parameters = default_parameters.copy()

    def __init__(self, x, y):
        """
        Parameters
        ----------
        x: int
            row number
        y: int
            column number
        """
        super().__init__(x, y)


class Lowland(Landscape):
    """Lowland (subclass)"""
    default_parameters = {'f_max': 800}
    current_parameters = default_parameters.copy()

    def __init__(self, x, y):
        """
        Parameters
        ----------
        x: int
            row number
        y: int
            column number
        """
        super().__init__(x, y)


class Desert(Landscape):
    """Desert (subclass)"""
    default_parameters = {'f_max': 0}
    current_parameters = default_parameters.copy()

    def __init__(self, x, y):
        """
        Parameters
        ----------
        x: int
            row number
        y: int
            column number
        """
        super().__init__(x, y)


class Water(Landscape):
    """Water (subclass). Has specific restrictions"""
    default_parameters = {'f_max': 0}
    current_parameters = default_parameters.copy()

    def __int__(self, x, y):
        """
        Parameters
        ----------
        x: int
            row number
        y: int
            column number
        """
        super().__init__(x, y)

    def add_animals(self, population):
        """
        Function that raises an error since water is an uninhabited area.

        Parameters
        ----------
        population: list
            List containing dictionaries containing the species type, age, and weight

        Raises
        -------
        TypeError
            Animals cannot exist in water.
        """
        raise TypeError('Herbivores cannot exist in ' + str(type(self)))
