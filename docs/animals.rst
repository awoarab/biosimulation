Animal
===================

The animal module
-------------------
.. automodule:: biosim.animals
   :members: Animal, Carnivore, Herbivore

