.. Modelling the Ecosystem of Rossumøya documentation master file, created by
   sphinx-quickstart on Tue Jan 24 10:13:20 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Modelling the Ecosystem of Rossumøya's documentation!
================================================================

This is a python package that simulates the life of animals on the Rossumoya island.
The main components that represent life on Rossumoya Island is: Animals, Landscape, Island

The main components that simulate and visualize life on the island is: Simulation, Graphics

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   animals
   landscape
   island
   simulation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
