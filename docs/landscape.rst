Landscape
===================

The landscape module
---------------------
.. automodule:: biosim.landscape
   :members: Landscape, Lowland, Highland, Water